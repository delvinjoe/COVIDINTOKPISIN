# COVIDINTOKPISIN
Free Awareness App in TOKPISIN - language commonly spoken by Papua New Guineans.

Data sources are from the WHO PNG Facebook page - https://www.facebook.com/WHOPapuaNewGuinea 
and the PNG Joint Agency Task Force National Control Centre for COVID-19 website - https://covid19.info.gov.pg/.
